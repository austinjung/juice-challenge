/**
 * Created by AustinSanghoon on 2017-05-04.
 */
app.controller('JuiceController', ['$scope', '$filter', 'Upload', function (scope, filter, Upload) {
    scope.transactions = null;
    scope.transaction_summary = null;
    scope.csv_error = false;

    scope.fileUpload = function($files) {
        scope.transactions = null;
        scope.transaction_summary = null;
        scope.csv_error = false;
        Upload.upload({
            url: '/',
            file: $files[0]
        }).progress(function(e) {
        }).then(function(data, status, headers, config) {
            // file is uploaded successfully
            scope.transactions = data.data.transactions;
            scope.transaction_summary = data.data.transaction_summary;
        }, function(error) {
            scope.csv_error = true;
            scope.csv_error_message = error.data.csv_error_message;
        })
    };
}]);