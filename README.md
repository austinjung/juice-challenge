# juice-challenge
Coding challenge for potential Juicers. Applicants need to complete the following coding challenge and submit the solution as instructed. If you have any questions, please email nectar-dev@juicemobile.com

## Task Description
You are asked to create a simple web application which imports data from a CSV file uploaded by the user. The CSV file will contain rows of transactions and each transaction has these columns: date, category and spend.

1. There is an example of the CSV file named `transactions.csv` included in this repo which you should use for testing. Note that there should always be a header line and the order of the columns is not guaranteed. Not all data are formatted consistently and you are expected to parse them correctly.  All rows are parseable using the capabilities associated with the Python programming language.  The final format of each row should be as follows:
Date should be formatted as "YYYY/MM/DD" and should successfully cast to a date data type.
Category should cast to valid string.
Spend should be formatted as a floating point number with two degrees of specificity beyond the decimal point.

2. Upon uploading the file, your app should display a table or a list of total spend per category.

3. Provide a search box for the user. When the user enters characters in the search box, the results should update by searching for transactions with categories partially matching the entered text. You may provide a button to trigger the search action.

## Live site on my cloud server

## Setup application
- [Install Python/Django and Verify](https://docs.djangoproject.com/en/1.10/intro/install/)
- [Install pip or upgrade pip](https://pip.pypa.io/en/stable/installing/)
- [Install virtualenv](https://virtualenv.pypa.io/en/stable/installation/)
- change directory to the project top folder which has manage.py and requirements.txt
- run 'pip install -r requirements.txt' to install all required packages for this application
- run 'python manage.py collectstatic' to collect all static files for this application
- run 'python manage.py runserver 0.0.0.0:8000' to run this application
- open web browser and visit 'http://localhost:8000'

## Live site
- [Juice Challenge at Austin's cloud server](http://juicemobile.somoon.info/)

## Design
- Using django open one url endpoint. ANd angular GUI will use this endpoint.
- Get 'http://localhost:8000' will response with HTML with angularjs.
- When select csv file, angular controller will post csv file to 'http://localhost:8000'.
- 'http://localhost:8000' will response with transaction summary and detail in json format.
- Because of this is simple application, I didn't implement any DB model like users, sessions, authentication... and transaction too.
- This application will response the summary and details of uploaded csv file using in-memory file. And don't store transactions data persistently.

## Project repositories
- [All these repositories are in Bitbucket. You can create free bitbucket account](https://bitbucket.org)
- [Juice Challenge](https://bitbucket.org/austinjung/juice-challenge)
- [Kaizen API is built using Django Rest Framework (DRF)](https://bitbucket.org/austinjung/kaizen_drf)
- [Django Rest Framework Presentation with demo project at CopperTree](https://bitbucket.org/austinjung/drf-demo)
- [Simple Wikipedia](https://bitbucket.org/austinjung/querywikipedia)
