from django.shortcuts import render_to_response
from django.http import JsonResponse, HttpResponseBadRequest
import types
import csv
import codecs
from datetime import datetime


# Create your views here.
def juice_challenge(request):
    transaction_summary_dict = {}
    transaction_summary = []
    transactions = []
    if request.FILES:
        csvfile = request.FILES['file']
        dialect = csv.Sniffer().sniff(codecs.EncodedFile(csvfile, "utf-8").read(1024))
        csvfile.open()
        reader = csv.reader(codecs.EncodedFile(csvfile, "utf-8"), delimiter=',', dialect=dialect)
        line_counter = 0
        for row in reader:
            if line_counter == 0:  #Header
                transaction_headers = row
                if set(['date', 'category', 'spend']).symmetric_difference(set(transaction_headers)) != set([]):
                    csv_error = True
                    csv_error_message = 'The csv file headers are not valid.'
                    context = {
                        'csv_error': csv_error,
                        'csv_error_message': csv_error_message
                    }
                    return JsonResponse(context, status=400)  # Bad request
            else:
                transaction = {}
                transaction_header_index = 0
                for column in row:
                    column = column.strip()
                    if transaction_headers[transaction_header_index] == 'spend':
                        try:
                            transaction[transaction_headers[transaction_header_index]] = float(column.replace('$', ''))
                        except ValueError:
                            transaction[transaction_headers[transaction_header_index]] = 'Invalid spend: %s' % column
                    elif transaction_headers[transaction_header_index] == 'date':
                        transaction[transaction_headers[transaction_header_index]] = datetime.strptime(column, '%m/%d/%Y')
                    else:
                        transaction[transaction_headers[transaction_header_index]] = column
                    transaction_header_index += 1
                transactions.append(transaction)
                if isinstance(transaction['spend'], types.FloatType):
                    if transaction['category'] not in transaction_summary_dict:
                        transaction_summary_dict[transaction['category']] = transaction['spend']
                    else:
                        transaction_summary_dict[transaction['category']] += transaction['spend']
            line_counter += 1
        for key, value in transaction_summary_dict.iteritems():
            transaction_summary.append({'category': key, 'spend': value})
        context = {
            'transactions': transactions,
            'transaction_summary': transaction_summary
        }
        return JsonResponse(context)
    else:
        context = {}
        return render_to_response('juice_challenge.html', context=context)
