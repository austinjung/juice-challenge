from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings
from JuiceChallenge.views import *

urlpatterns = [
    url(r'^$', juice_challenge, name='juice_challenge'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
